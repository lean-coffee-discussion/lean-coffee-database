#!/bin/sh

helpText () {
  echo "build-snap.sh SCRIPT_FILENAME OUTPUT_REPOSITORY [AUTHOR] {MESSAGE}"
}

if [ "$#" -lt 2 ]
then
	helpText
	exit 1
fi

if [ "$#" -gt 4 ]
then
	helpText
	exit 1
fi

if [ ! -f $1 ]
then
	echo No such input script
	exit 1
fi

CID=$(docker run -d buildertools/dynamodb-local)
BID=$(docker run -d -v ${PWD}:/usr/local/db-script --net container:$CID buildertools/dynamodb-snap-builder ./"$1")
docker wait $BID
echo "Build complete using: $1"
# echo "Logs of seed script:"
# docker logs $BID
docker stop $CID | true
echo Committing to repository: $2
ID=$(docker commit -a "$3" -m "$4" $CID $2)
echo Cleaning up...
docker rm -v $CID $BID | true
echo Done: $ID
