#!/bin/sh

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Users \
	--attribute-definitions AttributeName=email,AttributeType=S \
	--key-schema AttributeName=email,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Users \
	--item '{"email": {"S": "user1@example.com"}, "password": {"S": "$2a$05$iiq/y6cba.m8wsiQHmHR7e3Z9VFvzhxsdWXubn0G3HCM89I3dImNW"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Users \
	--item '{"email": {"S": "user2@example.com"}, "password": {"S": "$2a$05$iiq/y6cba.m8wsiQHmHR7e3Z9VFvzhxsdWXubn0G3HCM89I3dImNW"}}'

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Boards \
	--attribute-definitions AttributeName=id,AttributeType=S \
	--key-schema AttributeName=id,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--attribute-definitions AttributeName=id,AttributeType=S \
	--key-schema AttributeName=id,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--attribute-definitions AttributeName=id,AttributeType=S \
	--key-schema AttributeName=id,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

