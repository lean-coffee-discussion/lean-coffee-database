#!/bin/sh

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Users \
	--attribute-definitions AttributeName=email,AttributeType=S \
	--key-schema AttributeName=email,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Boards \
	--attribute-definitions AttributeName=id,AttributeType=S \
	--key-schema AttributeName=id,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--attribute-definitions AttributeName=id,AttributeType=S \
	--key-schema AttributeName=id,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb create-table \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--attribute-definitions AttributeName=id,AttributeType=S \
	--key-schema AttributeName=id,KeyType=HASH \
	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Users \
	--item '{"email": {"S": "testuser@example.com"}, "password": {"S": "$2a$05$iiq/y6cba.m8wsiQHmHR7e3Z9VFvzhxsdWXubn0G3HCM89I3dImNW"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Users \
	--item '{"email": {"S": "anothertestuser@example.com"}, "password": {"S": "$2a$05$iiq/y6cba.m8wsiQHmHR7e3Z9VFvzhxsdWXubn0G3HCM89I3dImNW"}}'

aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Boards \
	--item '{"id": {"S": "board-1"}, "title": {"S": "Test User - Board 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Boards \
	--item '{"id": {"S": "board-2"}, "title": {"S": "Test User - Board 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Boards \
	--item '{"id": {"S": "board-3"}, "title": {"S": "Another Test User - Board 1"}, "index": {"N": "2"}}'

aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-1"}, "title": {"S": "Discussion 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-2"}, "title": {"S": "Discussion 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-3"}, "title": {"S": "Discussion 3"}, "index": {"N": "2"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-4"}, "title": {"S": "Discussion 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-5"}, "title": {"S": "Discussion 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-6"}, "title": {"S": "Discussion 3"}, "index": {"N": "2"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-7"}, "title": {"S": "Discussion 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-8"}, "title": {"S": "Discussion 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Discussions \
	--item '{"id": {"S": "discussion-9"}, "title": {"S": "Discussion 3"}, "index": {"N": "2"}}'

aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-1"}, "title": {"S": "Topic 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-2"}, "title": {"S": "Topic 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-3"}, "title": {"S": "Topic 3"}, "index": {"N": "2"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-4"}, "title": {"S": "Topic 4"}, "index": {"N": "3"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-5"}, "title": {"S": "Topic 5"}, "index": {"N": "4"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-6"}, "title": {"S": "Topic 6"}, "index": {"N": "5"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-7"}, "title": {"S": "Topic 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-8"}, "title": {"S": "Topic 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-9"}, "title": {"S": "Topic 3"}, "index": {"N": "2"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-10"}, "title": {"S": "Topic 4"}, "index": {"N": "3"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-11"}, "title": {"S": "Topic 5"}, "index": {"N": "4"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-12"}, "title": {"S": "Topic 6"}, "index": {"N": "5"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-13"}, "title": {"S": "Topic 1"}, "index": {"N": "0"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-14"}, "title": {"S": "Topic 2"}, "index": {"N": "1"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-15"}, "title": {"S": "Topic 3"}, "index": {"N": "2"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-16"}, "title": {"S": "Topic 4"}, "index": {"N": "3"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-17"}, "title": {"S": "Topic 5"}, "index": {"N": "4"}}'
aws dynamodb put-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
	--table-name Topics \
	--item '{"id": {"S": "topic-18"}, "title": {"S": "Topic 6"}, "index": {"N": "5"}}'

aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Users \
  --key '{"email": {"S": "testuser@example.com"}}' \
  --update-expression "ADD boards :b" \
  --expression-attribute-values '{":b": {"SS":["board-1", "board-2"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Users \
  --key '{"email": {"S": "anothertestuser@example.com"}}' \
  --update-expression "ADD boards :b" \
  --expression-attribute-values '{":b": {"SS":["board-3"]}}'

aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Boards \
  --key '{"id": {"S": "board-1"}}' \
  --update-expression "ADD discussions :c" \
  --expression-attribute-values '{":c": {"SS":["discussion-1", "discussion-2", "discussion-3"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Boards \
  --key '{"id": {"S": "board-2"}}' \
  --update-expression "ADD discussions :c" \
  --expression-attribute-values '{":c": {"SS":["discussion-4", "discussion-5", "discussion-6"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Boards \
  --key '{"id": {"S": "board-3"}}' \
  --update-expression "ADD discussions :c" \
  --expression-attribute-values '{":c": {"SS":["discussion-7", "discussion-8", "discussion-9"]}}'

aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-1"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-1", "topic-2"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-2"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-3"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-3"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-4", "topic-5", "topic-6"]}}'

aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-4"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-7", "topic-8", "topic-9"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-5"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-10", "topic-11"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-6"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-12"]}}'

aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-7"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-13", "topic-14", "topic-15"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-8"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-16", "topic-17"]}}'
aws dynamodb update-item \
	--endpoint-url http://localhost:8000 \
	--region us-west-2 \
  --table-name Discussions \
  --key '{"id": {"S": "discussion-9"}}' \
  --update-expression "ADD topics :t" \
  --expression-attribute-values '{":t": {"SS":["topic-18"]}}'